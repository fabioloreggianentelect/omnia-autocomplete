import { Component, Input, forwardRef, OnInit, OnDestroy, Injector, DoCheck, ElementRef, ViewChild, HostBinding, AfterContentInit, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl, NG_VALUE_ACCESSOR, ControlValueAccessor, NgControl } from '@angular/forms';
import { debounceTime, tap, filter } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { MatAutocompleteSelectedEvent, MatFormFieldControl } from '@angular/material';
import { FocusMonitor } from '@angular/cdk/a11y';

@Component({
    selector: 'omnia-autocomplete',
    templateUrl: 'omnia-autocomplete.component.html',
    styleUrls: ['omnia-autocomplete.component.css'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => OmniaAutocompleteComponent),
            multi: true
        },
        {
            provide: MatFormFieldControl,
            useExisting: OmniaAutocompleteComponent
        }
    ],
    host: {
        '[id]': 'id',
        '[attr.aria-describedby]': 'describedBy',
    }
})
export class OmniaAutocompleteComponent implements OnInit, OnChanges, AfterContentInit, OnDestroy, DoCheck, ControlValueAccessor, MatFormFieldControl<any> {

    @ViewChild('autocomplete', { read: ElementRef }) container: ElementRef;
    static nextId = 0;

    @HostBinding() id = `omnia-autocomplete-${OmniaAutocompleteComponent.nextId++}`;

    public formControl: FormControl;

    public _value: any;

    get value(): any {
        return this._value;
    }

    set value(value) {
        this._value = value;
        this.onChange(value);

        if (value === null) {
            this.formControl.setValue(null);
        }

        this.onTouched();
        this.stateChanges.next();
    }

    public _placeholder: string;

    @Input()
    get placeholder() {
        return this._placeholder;
    }

    set placeholder(plh) {
        this._placeholder = plh;
        this.stateChanges.next();
    }

    public _required: boolean;

    @Input()
    get required() {
        return this._required;
    }

    set required(req) {
        this._required = req ? true : false;
        this.stateChanges.next();
    }

    public _disabled: boolean;

    @Input()
    get disabled() {
        return this._disabled;
    }

    set disabled(dis) {
        this._disabled = dis ? true : false
        this.stateChanges.next();
    }

    get empty() {
        const value = this.formControl.value;
        return value ? false : true;
    }

    @HostBinding('class.floating')
    get shouldLabelFloat() {
        return this.focused || !this.empty;
    }

    @HostBinding('attr.aria-describedby') describedBy = '';
    setDescribedByIds(ids: string[]) {
        this.describedBy = ids.join(' ');
    }

    @Input() debounceTime: number = 500;
    @Input() items: any[] = [];

    @Input() displayWith: ((value: any) => string) | null;

    @Input() maxLength: number | null;

    public loading: boolean;

    public selecting: boolean;

    public focused: boolean;

    public stateChanges = new Subject<void>();
    public ngControl: any;
    public controlType = 'autocomplete';
    public errorState = false;

    constructor(public elRef: ElementRef, public injector: Injector, private fm: FocusMonitor) {
        this.loading = false;
        this.selecting = false;

        this.formControl = new FormControl();

        this.fm.monitor(elRef.nativeElement, true).subscribe(origin => {
            this.focused = !!origin;
            this.stateChanges.next();
        });
    }

    ngOnInit() {
        this.ngControl = this.injector.get(NgControl);
        if (this.ngControl != null) { this.ngControl.valueAccessor = this; }

        this.formControl.valueChanges.pipe(
            debounceTime(this.debounceTime),
            filter((value) => {
                if (this.selecting || value === null) {
                    this.selecting = false;
                    return false;
                }
                return true;
            }),
            tap(() => this.loading = true),
        ).subscribe((value) => {
            this.value = value;
        });
    }

    ngAfterContentInit() {
        const value = this.ngControl.value;
        if (value) {
            this.selecting = true;
            this.formControl.setValue(value);
            this.items = [value];
        }
    }

    ngDoCheck(): void {
        if (this.ngControl) {
            this.errorState = this.ngControl.invalid && this.ngControl.touched && !this.focused;
            this.stateChanges.next();
        }
    }

    ngOnChanges(changes: SimpleChanges) {
        console.log(changes);
        const { items } = changes;
        if (items) {
            this.loading = false;
        }
    }

    ngOnDestroy() {
        this.stateChanges.complete();
        this.fm.stopMonitoring(this.elRef.nativeElement);
    }

    itemSelected(event: MatAutocompleteSelectedEvent): void {
        this.selecting = true;
        const item = {
            selected: true,
            value: event.option.value,
        }
        this.onChange(item);
        this.stateChanges.next();
    }

    onChange = (delta: any) => { };

    onTouched = () => { };

    writeValue(delta: any): void {
        this.value = delta;
    }

    registerOnChange(fn: (v: any) => void): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: () => void): void {
        this.onTouched = fn;
    }

    onContainerClick(event: MouseEvent) {
        /* if ((event.target as Element).tagName.toLowerCase() != 'div') {
            this.container.nativeElement.querySelector('div').focus();
        } */
    }
}