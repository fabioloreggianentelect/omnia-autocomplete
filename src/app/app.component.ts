import { Component } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith, debounceTime, switchMap, tap, finalize, delay } from 'rxjs/operators';
import { of } from 'rxjs/observable/of';

export interface State {
  flag: string;
  name: string;
  population: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {


  states: State[] = [
    {
      name: 'Arkansas',
      population: '2.978M',
      flag: 'https://upload.wikimedia.org/wikipedia/commons/9/9d/Flag_of_Arkansas.svg'
    },
    {
      name: 'California',
      population: '39.14M',
      flag: 'https://upload.wikimedia.org/wikipedia/commons/0/01/Flag_of_California.svg'
    },
    {
      name: 'Florida',
      population: '20.27M',
      flag: 'https://upload.wikimedia.org/wikipedia/commons/f/f7/Flag_of_Florida.svg'
    },
    {
      name: 'Texas',
      population: '27.47M',
      flag: 'https://upload.wikimedia.org/wikipedia/commons/f/f7/Flag_of_Texas.svg'
    }
  ];

  stateCtrl = new FormControl(this.states[0], [Validators.required]);
  isLoading: boolean = false;
  items: State[] = [];

  constructor() {
    this.stateCtrl.valueChanges.subscribe(value => {
      /*
        A selected option will always look like this

        {
          selected: true,
          value: {} //some_data_here
        }
      */

      console.log(value);

      if (value !== null && !value.selected) {
        this.apiCallMock(value).subscribe((items) => {
          this.items = items;
        });
      }
    });
  }

  public apiCallMock = (value: string): Observable<State[]> => {
    if (value) {
      const filterValue = value.toLowerCase();

      return of(this.states.filter(state => state.name.toLowerCase().indexOf(filterValue) === 0)).pipe(
        delay(1500),
      );
    }
    return of(this.states.slice()).pipe(delay(1500));
  }

  clear() {
    this.stateCtrl.setValue(null);
  }

  public displayWith(value: State): string {
    if (value) {
      return `${value.name} (${value.population})`;
    }
    return undefined;
  }
}
